let
  Prelude = ../deps/Prelude.dhall
let
  CF = ../deps/Containerfile.dhall

let
  BindistSpec: Type =
      { version : Text
      , triple : Text
      }

let
  Options =
    { Type = 
      { bindist : BindistSpec
      , destDir : Text
      }
    , default = {}
    }

let
  install: Options.Type -> CF.Type =
    \(opts: Options.Type) ->
      let
        url: Text = "https://github.com/llvm/llvm-project/releases/download/llvmorg-${opts.bindist.version}/clang+llvm-${opts.bindist.version}-${opts.bindist.triple}.tar.xz"

      in
        CF.run "install LLVM for bootstrap GHC"
        [ "curl -L ${url} | tar -xJC ."
        , "mkdir ${opts.destDir}"
        , "cp -R clang+llvm*/* ${opts.destDir}"
        , "rm -R clang+llvm*"
        , "${opts.destDir}/bin/llc --version"
        ]

let
  maybeInstallTo: Text -> Optional BindistSpec -> CF.Type =
    \(destDir: Text) -> \(opts: Optional BindistSpec) ->
      merge
      { Some = \(tv: BindistSpec) -> install { bindist = tv, destDir = destDir }
      , None = [] : CF.Type
      } opts

let
  setEnv: Text -> CF.Type =
    \(destDir: Text) ->
      CF.env (toMap
        { LLC = "${destDir}/bin/llc"
        , OPT = "${destDir}/bin/opt"
        })

in
{ Options = Options
, BindistSpec = BindistSpec
, install = install
, maybeInstallTo = maybeInstallTo
, setEnv = setEnv
}
