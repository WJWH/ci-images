-- Linter Linux Docker image

let
  Prelude = ../deps/Prelude.dhall
let
  Image = ../Image.dhall

let 
  docker_base_url: Text = "registry.gitlab.haskell.org/ghc/ci-images"

let images: List Image =
[ Image::
  { name = "linters"
  , runnerTags = [ "x86_64-linux" ]
  , jobStage = "build-derived"
  , needs = [ "x86_64-linux-deb10" ]
  , image =
      let installMypy: CF.Type =
          [ CF.Statement.User "root" ]
        # installPackages [ "python3-pip" ]
        # CF.run "installing mypy" [ "pip3 install mypy==0.701" ]

      let lintersCommit: Text =
        "b5650c094c98daec9416ceac92ca2cf3c099640f"
      let installLinters: CF.Type =
          [ CF.Statement.User "ghc" ]
        # CF.run "cloning linter repository"
          [ "git clone https://gitlab.haskell.org/ghc/git-haskell-org-hooks"
          , "git -C git-haskell-org-hooks checkout ${lintersCommit}"
          ]
        # CF.run "building linters"
          [ "cd git-haskell-org-hooks"
          , "$CABAL update"
          , "$CABAL install -w $GHC"
          ]
        # CF.env (toMap { PATH = "/home/ghc/.cabal/bin:$PATH" })

      in
          CF.from "${docker_base_url}/x86_64-linux-deb10:latest"
        # installMypy
        # installLinters
  }
]

in images
