-- Debian/Ubuntu Linux Docker images

let
  Prelude = ../deps/Prelude.dhall
let
  CF = ../deps/Containerfile.dhall
let
  Llvm = ../components/Llvm.dhall
let
  Ghc = ../components/Ghc.dhall
let
  HaskellTools = ../components/HaskellTools.dhall
let
  Cabal = ../components/Cabal.dhall
let
  Image = ../Image.dhall

let 
  docker_base_url: Text = "registry.gitlab.haskell.org/ghc/ci-images"

let
  coreBuildDepends: List Text =
    [ "zlib1g-dev"
    , "libtinfo-dev"
    , "libsqlite3-0"
    , "libsqlite3-dev"
    , "libgmp-dev"
    , "ca-certificates"
    , "g++"
    , "git"
    , "make"
    , "automake"
    , "autoconf"
    , "gcc"
    , "perl"
    , "python3"
    , "texinfo"
    , "xz-utils"
    , "lbzip2"
    , "bzip2"
    , "patch"
    , "openssh-client"
    , "sudo"
    , "time"
    , "jq"
    , "wget"
    , "curl"
    , "locales"
    -- For LLVM
    , "libtinfo5"
    -- DWARF libraries
    , "libdw1", "libdw-dev"
    -- For nofib
    , "valgrind"
    ]

let
  docsBuildDepends: List Text =
    [ "python3-sphinx"
    , "texlive-xetex"
    , "texlive-latex-extra"
    , "texlive-binaries"
    , "texlive-fonts-recommended"
    , "lmodern"
    ]

let debianBuildDepends: List Text =
    [ "texlive-generic-extra" ]

let ubuntuBuildDepends: List Text =
    [ "texlive-plain-generic" ]

let
  buildDepends: List Text = coreBuildDepends # docsBuildDepends

let
  installPackages: List Text -> CF.Type =
    \(pkgs: List Text) ->
        CF.arg (toMap { DEBIAN_FRONTEND = "noninteractive" })
      # CF.run "install build dependencies"
        [ "apt-get update"
        , "apt-get install --no-install-recommends -qy ${Prelude.Text.concatSep " " pkgs}"
        , "apt-get clean"
        , "rm -rf /var/lib/apt/lists/*"
        ]

-- Create a normal user
let
  createUserStep: CF.Type =
      CF.run "create user"
      [ "adduser ghc --gecos 'GHC builds' --disabled-password"
      , "echo 'ghc ALL = NOPASSWD : ALL' > /etc/sudoers.d/ghc"
      ]
    # [CF.Statement.User "ghc"]
    # CF.workdir "/home/ghc/"

let
  DebianImage =
    let
      type: Type =
        { name: Text
        , fromImage : Text
        , runnerTags : List Text
        , bootstrapLlvm : Optional Llvm.BindistSpec
        , bootstrapGhc : Ghc.BindistSpec
        , llvm : Optional Llvm.BindistSpec
        , cabalSource : Cabal.Type
        , extraPackages: List Text
        }

    let
      toDocker: type -> Image.Type = \(opts : type) ->
        let
          ghcDir: Text = "/opt/ghc/${opts.bootstrapGhc.version}"
        let
          bootLlvmDir: Text = "/opt/llvm-bootstrap"
        let
          llvmDir: Text = "/opt/llvm"
        let
          bootstrapLlvmConfigureOptions =
            merge { Some = \(_: Llvm.BindistSpec) -> [ "LLC=${bootLlvmDir}/bin/llc", "OPT=${bootLlvmDir}/bin/opt" ]
                  , None = [] : List Text
                  } opts.bootstrapLlvm
            
        let
          image =
            CF.from opts.fromImage
          # CF.env (toMap { LANG = "C.UTF-8" })
          # [ CF.Statement.Shell ["/bin/bash", "-o", "pipefail", "-c"] ]
          # installPackages (buildDepends # opts.extraPackages)

            -- install LLVM for bootstrap GHC
          # Llvm.maybeInstallTo bootLlvmDir opts.bootstrapLlvm

            -- install GHC
          # Ghc.install
              { bindist = opts.bootstrapGhc
              , destDir = ghcDir
              , configureOpts = bootstrapLlvmConfigureOptions
              }
          # CF.env (toMap { GHC = "${ghcDir}/bin/ghc" })

            -- install LLVM to be used by built compiler
          # Llvm.maybeInstallTo llvmDir opts.llvm
          # Llvm.setEnv llvmDir

            -- install cabal-install
          # Cabal.install opts.cabalSource

            -- install hscolour, alex, and happy
          # HaskellTools.build

          # createUserStep
          # CF.run "update cabal index" [ "$CABAL update"]
          # [ CF.Statement.Cmd ["bash"] ]

        in Image::{ name = opts.name, runnerTags = opts.runnerTags, image = image }
    in
    { Type = type
    , toDocker = toDocker
    }

let images: List Image.Type =
[ DebianImage.toDocker
    { name = "aarch64-linux-deb10"
    , fromImage = "arm64v8/debian:buster"
    , runnerTags = [ "aarch64-linux" ]
    , bootstrapLlvm = Some { version = "9.0.1" , triple = "aarch64-linux-gnu" }
    , bootstrapGhc = { version = "8.10.2", triple = "aarch64-deb10-linux" }
    , llvm = Some { version = "10.0.1" , triple = "aarch64-linux-gnu" }
    , cabalSource = Cabal.Type.FromBindist "http://home.smart-cactus.org/~ben/ghc/cabal-install-3.2.0.0-aarch64-debian9-linux.tar.xz"
    , extraPackages = [ "libnuma-dev" ] : List Text
    }

, DebianImage.toDocker
    { name = "armv7-linux-deb10"
    , fromImage = "arm32v7/debian:buster"
    , runnerTags = [ "armv7-linux" ]
    , bootstrapLlvm = Some { version = "9.0.1", triple = "armv7a-linux-gnueabihf" }
    , bootstrapGhc = { version = "8.10.2", triple = "armv7-deb10-linux" }
    , llvm = Some { version = "10.0.1" , triple = "armv7a-linux-gnueabihf" }
    , cabalSource = Cabal.Type.FromBindist "http://home.smart-cactus.org/~ben/ghc/cabal-install-3.2.0.0-armv7l-debian10-linux.tar.xz"
    , extraPackages = [ "libnuma-dev" ] # debianBuildDepends
    }

, DebianImage.toDocker
    { name = "i386-linux-deb9"
    , fromImage = "i386/debian:stretch"
    , runnerTags = [ "x86_64-linux" ]
    , bootstrapLlvm = None Llvm.BindistSpec
    , bootstrapGhc = { version = "8.8.3", triple = "i386-deb9-linux" }
    , llvm = None Llvm.BindistSpec
    , cabalSource = Cabal.Type.FromBindist "https://downloads.haskell.org/~cabal/cabal-install-3.2.0.0/cabal-install-3.2.0.0-i386-unknown-linux.tar.xz"
    , extraPackages = debianBuildDepends # [ "cabal-install" ] : List Text
    }

-- N.B. Need bindist for deb10 i386
--, DebianImage.toDocker
--    { name = "i386-linux-deb10"
--    , fromImage = "i386/debian:buster"
--    , runnerTags = [ "x86_64-linux" ]
--    , bootstrapLlvm = None Llvm.BindistSpec
--    , bootstrapGhc = { version = "8.8.3", triple = "i386-deb10-linux" }
--    , llvm = None Llvm.BindistSpec
--    , cabalSource = Cabal.Type.FromSource "3.2.0.0"
--    , extraPackages =
--        debianBuildDepends
--      # [ "libnuma-dev" ]
--      # [ "cabal-install" ]
--    }

, DebianImage.toDocker
    { name = "x86_64-linux-deb10"
    , fromImage = "amd64/debian:buster"
    , runnerTags = [ "x86_64-linux" ]
    , bootstrapLlvm = None Llvm.BindistSpec
    , bootstrapGhc = { version = "8.8.3", triple = "x86_64-deb10-linux" }
    , llvm = Some { version = "10.0.1" , triple = "x86_64-linux-gnu-ubuntu-16.04" }
    , cabalSource = Cabal.fromUpstreamBindist { version = "3.2.0.0", triple = "x86_64-unknown-linux" }
    , extraPackages =
          debianBuildDepends
        -- For cross-compilation testing
        # ["crossbuild-essential-arm64"]
        # [ "libnuma-dev" ]
    }

, DebianImage.toDocker
    { name = "x86_64-linux-deb9"
    , fromImage = "amd64/debian:stretch"
    , runnerTags = [ "x86_64-linux" ]
    , bootstrapLlvm = None Llvm.BindistSpec
    , bootstrapGhc = { version = "8.8.3", triple = "x86_64-deb9-linux" }
    , llvm = Some { version = "10.0.1" , triple = "x86_64-linux-gnu-ubuntu-16.04" }
    , cabalSource = Cabal.fromUpstreamBindist { version = "3.2.0.0", triple = "x86_64-unknown-linux" }
    , extraPackages = debianBuildDepends : List Text
    }

, DebianImage.toDocker
    { name = "x86_64-linux-deb8"
    , fromImage = "amd64/debian:jessie"
    , runnerTags = [ "x86_64-linux" ]
    , bootstrapLlvm = None Llvm.BindistSpec
    , bootstrapGhc = { version = "8.8.3", triple = "x86_64-deb8-linux" }
    , llvm = None Llvm.BindistSpec
    , cabalSource = Cabal.fromUpstreamBindist { version = "3.2.0.0", triple = "x86_64-unknown-linux" }
    , extraPackages = debianBuildDepends : List Text
    }

, DebianImage.toDocker
    { name = "x86_64-linux-ubuntu20_04"
    , fromImage = "amd64/ubuntu:20.04"
    , runnerTags = [ "x86_64-linux" ]
    , bootstrapLlvm = None Llvm.BindistSpec
    , bootstrapGhc = { version = "8.8.3", triple = "x86_64-deb9-linux" }
    , llvm = Some { version = "10.0.1" , triple = "x86_64-linux-gnu-ubuntu-16.04" }
    , cabalSource = Cabal.fromUpstreamBindist { version = "3.2.0.0", triple = "x86_64-unknown-linux" }
    , extraPackages = ubuntuBuildDepends
    }

, Image::
  { name = "linters"
  , runnerTags = [ "x86_64-linux" ]
  , jobStage = "build-derived"
  , needs = [ "x86_64-linux-deb10" ]
  , image =
      let installMypy: CF.Type =
          [ CF.Statement.User "root" ]
        # installPackages [ "python3-pip" ]
        # CF.run "installing mypy" [ "pip3 install mypy==0.701" ]

      let lintersCommit: Text =
        "b5650c094c98daec9416ceac92ca2cf3c099640f"
      let installLinters: CF.Type =
          [ CF.Statement.User "ghc" ]
        # CF.run "cloning linter repository"
          [ "git clone https://gitlab.haskell.org/ghc/git-haskell-org-hooks"
          , "git -C git-haskell-org-hooks checkout ${lintersCommit}"
          ]
        # CF.run "building linters"
          [ "cd git-haskell-org-hooks"
          , "$CABAL update"
          , "$CABAL install -w $GHC"
          ]
        # CF.env (toMap { PATH = "/home/ghc/.cabal/bin:$PATH" })

      in
          CF.from "${docker_base_url}/x86_64-linux-deb10:latest"
        # installMypy
        # installLinters
  }
]
in images
